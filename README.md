[![pipeline status](https://gitlab.com/newtonkiragu/newtonkiragu.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/newtonkiragu/newtonkiragu.gitlab.io/-/commits/master)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

---

My Personal Blog.  View it live at https://newtonkaranu.me/blog/

---
